<?php

header('Content-type: application/json');

require("jwt/src/JWT.php");

$key = "testkey";

$payload = array(
    "iss" => "http://example.org",
    "aud" => "http://example.com",
    "iat" => 1356999524,
    "nbf" => 1357000000,
	"user" => "admin",
	"pwd" => "admin@12334"
);

$jwt = JWT::encode($payload, $key);

echo json_encode(["status" => true, "token" => $jwt]);