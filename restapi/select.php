<?php

/*
SELECT = GET
DELETE = DELETE
INSERT = POST
UPDATE = PUT

*/

header('Content-type: application/json');

require("../functions.php");
require("jwt/src/JWT.php");

$key = "testkey";

$jwt = $_SERVER["HTTP_JWT"];

$decoded = JWT::decode($jwt, $key, array('HS256'));



if(($_SERVER["PHP_AUTH_USER"] == "admin" && $_SERVER["PHP_AUTH_PW"] == "admin@123") || 
	$decoded->user == "admin" && $decoded->pwd == "admin@123") {
	
	if($_SERVER["REQUEST_METHOD"] == "GET") {
		$conn = getConn();

		$id = $_REQUEST["id"];

		$sql = "SELECT * FROM users WHERE id = $id LIMIT 1";

		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0) {
			$row = mysqli_fetch_assoc($result);
			echo json_encode(["status" => true, "data" => $row]);	
		} else {
			echo json_encode(["status" => false, "msg" => "No Records Found"]);
		}

	} else {
			
		echo json_encode(["status" => false, "msg" => "Invalid method"]);
	}
	
} else {
	echo json_encode(["status" => false, "msg" => "Unauthorized User, Jaa pehle us aadmi ka sign lekar aa jisne tujhe ye REST API diya hai"]);
}


?>