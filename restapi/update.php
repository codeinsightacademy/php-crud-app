<?php

header('Content-type: application/json');

require("../functions.php");

$conn = getConn();

if($_SERVER["REQUEST_METHOD"] == "PUT") {

	// Takes raw data from the request
	$json = file_get_contents('php://input');

	// Converts it into a PHP object
	$data = json_decode($json);
	
	$id = $data->id;
	$name = $data->name;
	$age = $data->age;
	$city = $data->city;
	$status = $data->status;

	$sql = "UPDATE `users` SET name = '$name', `age` = '$age', city = '$city ', status='$status', updated_date = NOW() WHERE `users`.`id` = $id";

	$result = mysqli_query($conn, $sql);

	if($result) {
		
		echo json_encode(["status" => true, "msg" => "Record has been updated successfully"]);
		
	} else {
		
		echo json_encode(["status" => false, "msg" => "Something went wrong, please contact developer"]);
		
	}
} else {
		
	echo json_encode(["status" => false, "msg" => "Invalid method"]);
}
?>