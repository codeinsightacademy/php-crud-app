<?php

header('Content-type: application/json');

require("../functions.php");

$conn = getConn();

if($_SERVER["REQUEST_METHOD"] == "POST") {

	// Takes raw data from the request
	$json = file_get_contents('php://input');

	// Converts it into a PHP object
	$data = json_decode($json);

	$username = $data->username;
	$password = $data->password;
	$name = $data->name;
	$age = $data->age;
	$city = $data->city;

	$sql = "INSERT INTO `users` (`id`, `username`, `password`, `name`, `age`, `city`, `added_date`, `updated_date`, `status`) VALUES (NULL, '$username', '$password', '$name', '$age', '$city', NOW(), NOW(), 'active');";

	$result = mysqli_query($conn, $sql);

	if($result) {
		
		echo json_encode(["status" => true, "msg" => "Record has been added successfully", "id" => mysqli_insert_id($conn)]);
		
	} else {
		
		echo json_encode(["status" => false, "msg" => "Something went wrong, please contact developer"]);
		
	}
	
} else {
		
	echo json_encode(["status" => false, "msg" => "Invalid method"]);
}
?>