<?php

header('Content-type: application/json');

require("../functions.php");

$conn = getConn();


if($_SERVER["REQUEST_METHOD"] == "DELETE") {

	// Takes raw data from the request
	$json = file_get_contents('php://input');

	// Converts it into a PHP object
	$data = json_decode($json);

	$id = $data->id;

	$sql = "DELETE FROM users WHERE id = $id";

	$result = mysqli_query($conn, $sql);
	
	echo json_encode(["status" => true, "msg" => "Record has been deleted"]);	
	
} else {
		
	echo json_encode(["status" => false, "msg" => "Invalid method"]);
}
?>