# PhpCrudapp

How to create website using Core Php

Points to be Covered

**1. Installation and Hello World Program**
	check all requrired software
* 	    apache
* 		mysql
* 		phpmyadmin
* 		notepad++
* 		command prompt and gitbash

		
**2. Database Design and Add Record Manually**
*   create database
* 	create table admin and users
* 	add dummy data in table

	
**3. Create header File**
*   create header file
* 	create database connection function in header file
* 	include this header file using require or include function


**4. Session Management Login/Logout Functionality**
*   create login_action.php
* 	write hardcode to check whether admin is exist or not 
* 	if admin is available check authentication
* 	if admin is authenticated then create session variable and add adminid/adminusername in session
* 	create autherization function in header file
* 	create login_form.php and make the login functionality dynamic
* 	create logout.php and write destroy session in this file
* 	once the session is destroyed in logout.php function then it should redirect to login_form.php

	
**5. Dashboard Listing and Delete records**
*   check admin authorization
* 	if admin is authorized then display dashboard else redirect to login_form.php
* 	use database connection function and select data from database table users
* 	add Action column which will contain Edit and Delete hyperlink
* 	create delete.php script which will have delete sql 
* 	delete record by using where clause e.g. "WHERE id=5" [NOTE: 5 (dynamic id) should come from query parameter]
* 	once the record gets deleted then redirect admin to dashboard listing page
* 	add onclick attribute and confirm box to Delete anchor tag and make delete functionality dynamic 


**6. Add Records**
*   create add_action.php script 
* 	write insert sql logic in this script with hard coded data
* 	once the record get inserted in database table users then redirect it to dashboard page
* 	create hyperlink Add Record in dashboard.php page which will take admin to add_form.php 
* 	make add record functionality dynmic by creating form in add_form.php and keep its action to add_action.php

	
**7. Edit Records**
*   create edit_action.php script
* 	write update sql logic in this script with hard coded data
* 	once the record get udpated make it dynamic by taking all data from query parameters
* 	if script run successfully and update record it should redirect to dashboard.php
* 	create edit_form.php
* 	add query parameter in edit_form.php hyperlink available in dashboard.php file
* 	select data from users table on basis of id value coming from query paramter and fill data in form
* 	test the update functionality by submitting the edit from

	
**8. File Handling**
*   create download csv script to download all records in csv format
* 	use fputcsv function and add header information to download it from browser
* 	create hyperlink download csv in dashboard.php page
* 	once user click on this link it should execute downloadcsv.php script

	
**9. Web Services**
*   create sample webservice and test it using postman tool
	
**10. Security VAPT**
*   discussion
* 	What is VAPT
* 	What is SQL Injection
* 	What is CSRF Token
* 	What is Basic auth and JWT token
* 	What is OAuth 2

	
