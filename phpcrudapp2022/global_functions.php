<?php

function authorize() {

	if(!isset($_SESSION['username'])) {
		header("Location: login_form.php?msg=Unauthorised access");
		exit;
	}

}
