<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<a class="btn btn-primary" style="float: right;" href="logout.php">Logout</a>
<?php
require("dbconn.php");
require("global_functions.php");
session_start();

authorize();

echo "<h1>Hello " . $_SESSION['username'] . "</h1>";

?>

<a class="btn btn-primary" href="add_form.php">Add User</a>

<?php

$sql = "SELECT * FROM users";
error_log("==============SELECT SQL: $sql===================\n\n");

$stmt = $conn->prepare($sql);
$stmt->execute();

$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$records = $stmt->fetchAll();

?>


<table  class="table table-striped table-hover">
	<thead>
		<th>Id</th>
		<th>Name</th>
		<th>Age</th>
		<th>City</th>
		<th>Action</th>
	</thead>
	<tbody>
		<?php foreach($records as $user) { ?>
		<tr>
			<td><?php echo $user['id']; ?></td>
			<td><?php echo $user['name']; ?></td>
			<td><?php echo $user['age']; ?></td>
			<td><?php echo $user['city']; ?></td>
			<td>
			<a class="btn btn-info" href="edit_form.php?id=<?php echo $user['id']; ?>">Edit</a>
		<?php if($_SESSION['username'] == 'superadmin') { ?>
			<a class="btn btn-danger" onclick="return confirm('Are you sure want to delete record of <?php echo $user['name']; ?>?');" href="delete.php?id=<?php echo $user['id']; ?>">Delete</a>
		<?php } ?>
			</td>
		</tr>


		<?php } ?>
	</tbody>
</table>
